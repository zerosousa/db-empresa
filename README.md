## Name
db-empresa
## Description
Estrutura de banco de dados com script para criação de database e eschemas para um exemplo de aplicação para empresa com gestão de pessoas, estoque e vendas, além de ferramenta de comunicação.
Ordem das etapas:
- Levantamento conceitual
- Levantamento lógico
- Modelo físico

Problema dado:
"Uma empresa do ramo do vestuário possui 3 setores: produção, administração e varejo. 
Essa empresa necessita um sistema para gerenciar os dados dos 3 setores de forma integrada e ainda uma ferramenta para comunicação interna.
No setor de produção é necessário controlar os produtos produzidos e sua matéria prima utilizada.
No setor administrativo é necessário possuir um gerenciamento de pessoas, sejam funcionários e/ou clientes e suas funções na empresa.
No setor de varejo é necessário possuir um controle de vendas e estoque.
Ainda se faz necessário a produção de relatórios.
A ferramenta de comunicação deve permitir que qualquer pessoa na empresa, seja da alta gestão ou simples funcionário, possa publicar notificações, novidades, anúncios, recados, notícias, convites, etc. 
Deve funcionar como um feed de publicações similar a um workspace."
## Installation
SGBD PostgreSQL
db: empresa
schemas: gestao e comunicacao
## Usage
Executar os comandos de criação e população nas tabelas na ordem que aparecem no modelo físico.
## Roadmap
Implantar procedures e triggers para popular as tabelas de venda e cupom.