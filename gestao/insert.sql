-- # arquivo com comandos para popular a base da área de gestao
-- # Carlos Sousa 17 Mar 2022

-- produção
insert into gestao.materia_prima (de_materia_prima) values ('couro');
insert into gestao.materia_prima (de_materia_prima) values ('algodao');

insert into gestao.produto (de_produto, id_materia_prima) values ('jaqueta', 1);
insert into gestao.produto (de_produto, id_materia_prima) values ('camiseta', 2);

insert into gestao.producao_diaria (id_produto, dt_data_producao, nu_quantidade_produzida) values (3, now(),300);
insert into gestao.producao_diaria (id_produto, dt_data_producao, nu_quantidade_produzida) values (4, now(),500);

-- administração
insert into gestao.cargo (id_cargo, de_desc_cargo) values (1, 'CHEFE');
insert into gestao.cargo (id_cargo, de_desc_cargo) values (2, 'PEAO');

insert into gestao.funcionarios (cd_cpf_funcionario, nm_nome_funcionario, id_cargo) values ('12345678911', 'JOSE DAS COUVES', 1);
insert into gestao.funcionarios (cd_cpf_funcionario, nm_nome_funcionario, id_cargo) values ('11987624321', 'ENZO MIGUEL', 2);

-- varejo
insert into gestao.clientes (cd_cpf_cliente, nm_nome_cliente, nm_informacoes) values ('00000000000', 'CLIENTE NAO CADASTRADO', 'REGISTRO PARA CLIENTES SEM CADASTRO');
insert into gestao.clientes (cd_cpf_cliente, nm_nome_cliente, nm_informacoes) values ('12345678911', 'JOSE DAS COUVES', 'CASA ENGRACADA SEM TETO SEM NADA');
insert into gestao.clientes (cd_cpf_cliente, nm_nome_cliente, nm_informacoes) values ('13579111315', 'CLIENTE FAVORITO', 'VILA RICA');

insert into gestao.estoque (id_produto_estoque, nu_quantidade_estoque, vl_valor_produto) values (3, 300, 60.00);
insert into gestao.estoque (id_produto_estoque, nu_quantidade_estoque, vl_valor_produto) values (4, 500, 30.00);

insert into gestao.vendas (cd_cpf_cliente, dt_data_venda) values ('00000000000', now());
insert into gestao.vendas (cd_cpf_cliente, dt_data_venda) values ('12345678911', now());
insert into gestao.vendas (cd_cpf_cliente, dt_data_venda) values ('13579111315', now());

insert into gestao.cupom (id_produto_estoque, nu_quantidade_vendida, vl_valor_venda, id_venda) values (3, 5, 300.00, 5);
insert into gestao.cupom (id_produto_estoque, nu_quantidade_vendida, vl_valor_venda, id_venda) values (4, 10, 300.00, 5);
insert into gestao.cupom (id_produto_estoque, nu_quantidade_vendida, vl_valor_venda, id_venda) values (3, 1, 60.00, 6);
insert into gestao.cupom (id_produto_estoque, nu_quantidade_vendida, vl_valor_venda, id_venda) values (4, 2, 60.00, 6);
insert into gestao.cupom (id_produto_estoque, nu_quantidade_vendida, vl_valor_venda, id_venda) values (3, 1, 60.00, 7);
