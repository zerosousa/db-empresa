-- # arquivo com primeira abordagem fisica
-- # Carlos Sousa 17 Mar 2022

-- 3) Físico ---------

CREATE SCHEMA gestao;


CREATE TABLE gestao.materia_prima (
	id_materia serial8 NOT NULL,
	de_materia_prima varchar NULL,
	CONSTRAINT pk_materiaprima PRIMARY KEY (id_materia)
);


CREATE TABLE gestao.produto (
	id_produto serial8 NOT NULL,
	de_produto varchar NULL,
	id_materia_prima int8 NULL,
	CONSTRAINT pk_produto PRIMARY KEY (id_produto),
	CONSTRAINT produto_fk FOREIGN KEY (id_materia_prima) REFERENCES gestao.materia_prima(id_materia)
);


CREATE TABLE gestao.producao_diaria (
	id_produto int8 NULL,
	dt_data_producao date NULL,
	nu_quantidade_produzida varchar NULL,
	CONSTRAINT pk_producao_diaria PRIMARY KEY (id_produto,dt_data_producao),
	CONSTRAINT producao_diaria_fk FOREIGN KEY (id_produto) REFERENCES gestao.produto(id_produto)
);


CREATE TABLE gestao.cargo (
	id_cargo bigserial NOT NULL,
	de_desc_cargo varchar NULL,
	CONSTRAINT pk_cargo PRIMARY KEY (id_cargo)
);


CREATE TABLE gestao.funcionarios (
	cd_cpf_funcionario char(11) NULL,
	nm_nome_funcionario varchar NULL,
	id_cargo int8 NULL,
	CONSTRAINT pk_funcionarios PRIMARY KEY (cd_cpd_funcionarios),
	CONSTRAINT funcionarios_fk FOREIGN KEY (id_cargo) REFERENCES gestao.cargo(id_cargo)
);


CREATE TABLE gestao.clientes (
	cd_cpf_cliente char(11) NULL,
	nm_nome_cliente varchar NULL,
	nm_informacoes varchar NULL,
	CONSTRAINT pk_clientes PRIMARY KEY (cd_cpf_cliente)
);


CREATE TABLE gestao.estoque (
	id_produto_estoque int8 NULL,
	nu_quantidade_estoque int8 NULL,
	vl_valor_produto decimal NULL,
	CONSTRAINT estoque_pk PRIMARY KEY (id_produto_estoque),
	CONSTRAINT estoque_fk FOREIGN KEY (id_produto_estoque) REFERENCES gestao.produto(id_produto)
);


CREATE TABLE gestao.cupom (
	id_cupom bigserial NOT NULL,
	id_produto_estoque int8 NULL,
	nu_quantidade_vendida int8 NULL,
	vl_valor_venda numeric NULL,
	id_venda int8 NULL,
	CONSTRAINT pk_cupom PRIMARY KEY (id_cupom),
	CONSTRAINT cupom_fk FOREIGN KEY (id_produto_estoque) REFERENCES gestao.estoque(id_produto_estoque),
	CONSTRAINT cupom_fk2 FOREIGN KEY (id_venda) REFERENCES gestao.vendas(id_venda)
);


CREATE TABLE gestao.vendas (
	id_venda bigserial NOT NULL,
	cd_cpf_cliente bpchar(11) NULL,
	vl_valor_total_venda numeric NULL,
	dt_data_venda date NOT NULL,
	CONSTRAINT pk_vendas PRIMARY KEY (id_venda),
	CONSTRAINT vendas_fk FOREIGN KEY (cd_cpf_cliente) REFERENCES gestao.clientes(cd_cpf_cliente)
);


-- Views para relatório

CREATE OR REPLACE VIEW gestao.vw_valor_vendas_diaria
AS SELECT v.dt_data_venda,
    sum(v.vl_valor_total_venda) AS sum
   FROM gestao.vendas v
  GROUP BY v.dt_data_venda
  ORDER BY v.dt_data_venda;


CREATE OR REPLACE VIEW gestao.vw_valor_vendas_por_cliente
AS SELECT v.cd_cpf_cliente,
    sum(v.vl_valor_total_venda) AS sum
   FROM gestao.vendas v
  GROUP BY v.cd_cpf_cliente
  ORDER BY v.cd_cpf_cliente;


CREATE OR REPLACE VIEW gestao.vw_vendas_por_cliente_data
AS SELECT v.cd_cpf_cliente,
    v.dt_data_venda,
    sum(v.vl_valor_total_venda) AS sum
   FROM gestao.vendas v
  GROUP BY v.cd_cpf_cliente, v.dt_data_venda
  ORDER BY v.cd_cpf_cliente;
