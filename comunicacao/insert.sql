-- # arquivo com comandos para popular a base da área de comunicacao
-- # Carlos Sousa 17 Mar 2022

insert into comunicacao.tipo_publicacao (de_publicacao) values ('NOTIFICACAO');
insert into comunicacao.tipo_publicacao (de_publicacao) values ('ANUNCIO');
insert into comunicacao.tipo_publicacao (de_publicacao) values ('RECADO');

insert into comunicacao.publicacao (cd_cpf_funcionario, tp_publicacao, de_conteudo, dt_data_publicacao) values (12345678911, 1, 'MINHA NOTIFICACAO', now());
insert into comunicacao.publicacao (cd_cpf_funcionario, tp_publicacao, de_conteudo, dt_data_publicacao) values (11987624321, 1, 'MEU ANUNCIO', now());
insert into comunicacao.publicacao (cd_cpf_funcionario, tp_publicacao, de_conteudo, dt_data_publicacao) values (11987624321, 1, 'MEU RECADO', now());




