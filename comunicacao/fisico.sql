-- # arquivo com primeira abordagem fisica
-- # Carlos Sousa 17 Mar 2022

CREATE SCHEMA comunicacao;

CREATE TABLE comunicacao.tipo_publicacao (
	id_tp_publiccao serial8 NOT NULL,
	de_publicacao varchar NULL,
	CONSTRAINT pk_tipo_publicacao PRIMARY KEY (id_tp_publiccao)
);


CREATE TABLE comunicacao.publicacao (
	id_publicacao bigserial NOT NULL,
	cd_cpf_funcionario bpchar(11) NULL,
	tp_publicacao int8 NULL,
	de_conteudo varchar NULL,
	dt_data_publicacao date NULL,
	CONSTRAINT pk_publicacao PRIMARY KEY (id_publicacao),
	CONSTRAINT publicacao_fk FOREIGN KEY (tp_publicacao) REFERENCES comunicacao.tipo_publicacao(id_tp_publiccao),
	CONSTRAINT publicacao_fk_1 FOREIGN KEY (cd_cpf_funcionario) REFERENCES gestao.funcionarios(cd_cpd_funcionarios)
);

